#!/usr/bin/python
import cgi, cgitb
cgitb.enable()

import sys
sys.path.append('/home2/transvar/tools/transvar')
sys.path.append('/home2/transvar/tools/python/SQLAlchemy-0.9.8-py2.6-linux-x86_64.egg')

from revanno import add_parser_revanno
from anno import add_parser_anno
from codonsearch import add_parser_codonsearch
from config import add_parser_config, read_config
import re, argparse
from itertools import chain
from cStringIO import StringIO

class Capturing(list):
    def __enter__(self):
        self._stdout = sys.stdout
        sys.stdout = self._stringio = StringIO()
        return self
    def __exit__(self, *args):
        self.extend(self._stringio.getvalue().splitlines())
        sys.stdout = self._stdout

parser = argparse.ArgumentParser(description=__doc__)
config = read_config()
subparsers = parser.add_subparsers()
add_parser_revanno(subparsers, config)
add_parser_anno(subparsers, config)
add_parser_codonsearch(subparsers, config)
add_parser_config(subparsers)

form = cgi.FieldStorage()

import json
response = []
with Capturing() as response:
    inputtext = '\n'.join([form.getvalue('input_text'), form.getvalue('inputListFile')])
    comm = [form.getvalue('task'), '--sql'] # '-i', form.getvalue('in_id'), 
    if form.getvalue('checkensembl'):
        comm.append('--ensembl')
    if form.getvalue('checkccds'):
        comm.append('--ccds')
    if form.getvalue('checkrefseq'):
        comm.append('--refseq')
    if form.getvalue('checkgencode'):
        comm.append('--gencode')
    if form.getvalue('checkucsc'):
        comm.append('--ucsc')
    if form.getvalue('checkaceview'):
        comm.append('--aceview')
    if form.getvalue('version') == 'hg38':
        comm.extend(['--refversion', 'hg38'])
    args = parser.parse_args(comm)
    args.l = iter(inputtext.splitlines())
    args.func(args)

print "Content-type: application/json\r\n\r\n"
print(json.JSONEncoder().encode(response))

