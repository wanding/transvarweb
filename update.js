var successHandler = function(data) {
    var res = $("#resultDisplay");
    res.html("");
    for (var i = 0; i < data.length; i++) {
	res.append($.trim(data[i])+"<br>");
    }
}

var main = function() {
    $("button#submitButton").click(function() {
	var fd = new FormData(document.getElementById("inputform"));
	fd.append("inputListFile", $('#input-file')[0].files[0]);
	
	$.ajax({
      	url: "cgi-bin/transvar.cgi",
	    data: fd,
	    dataType: 'json',
	    contentType: false,
	    processData: false,
	    type: 'POST',
	    success: successHandler,
	})
	//$("#resultDisplay").text("something");
	//$('#resultDisplay').html('som')

	// $.post("cgi-bin/transvar.cgi", $('form#tvinputform').serialize(), function(data) {
	//     // alert(data.length);
	//     var res = $("#resultBak");
	//     res.html("");
	//     for (var i = 0; i < data.length; i++) {
	// 	res.append($.trim(data[i])+"<br>");
	//     }
	// }, 'json');
    });
    // $("input#submitButtonFor").click(function() {
    // 	    $.post("cgi-bin/transvar.cgi", $('form#TVInputFormFor').serialize(), function(data) {
    // 		    // alert(data.length)
    // 		    var res = $("#resultFor");
    // 		    res.html("");
    // 		    for (var i = 0; i < data.length; i++) {
    // 			res.append($.trim(data[i])+"<br>");
    // 		    }
    // 		},
    // 		'json');
    // 	});
    // $("input#submitButtonSearch").click(function() {
    // 	    $.post("cgi-bin/transvar.cgi", $('form#TVInputFormSearch').serialize(), function(data) {
    // 		    // alert(data.length)
    // 		    var res = $("#resultSearch");
    // 		    res.html("");
    // 		    for (var i = 0; i < data.length; i++) {
    // 			res.append($.trim(data[i])+"<br>");
    // 		    }
    // 		},
    // 		'json');
    // });


    $(".btn-file :file").on("fileselect", function(event, numFiles, label) {
    	var input = $(this).parents(".input-group").find(":text"),
    	    log = numFiles > 1 ? numFiles + " files selected" : label;
    	if (input.length) {
    	    input.val(log);
    	} else {
    	    if (log) alert(log);
    	}
    });
}
$(document).ready(main);

$(document).on("change", ".btn-file :file", function() {
    var input = $(this),
	numFiles = input.get(0).files ? input.get(0).files.length : 1,
	label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger("fileselect", [numFiles, label]);
})
